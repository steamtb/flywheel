package main

import (
	"log"
	"sync"
)

type Runtime struct {
	Components []Component
	Links      []*Link
	Clock      uint64
	Stop       chan bool
}

func NewRuntime() *Runtime {

	return &Runtime{Stop: make(chan bool, 1000)}

}

func (r *Runtime) Add(c Component) Component {
	r.Components = append(r.Components, c)
	return c
}

func (r *Runtime) Connect(
	a, b Component, delay uint64, width uint, freq uint64) *Link {

	interval := uint64(1e12 / freq)
	ai := NewInterface()
	ai.Width = width
	ai.interval = interval
	ai.Parent = a.Internal()
	ai.Index = 0

	bi := NewInterface()
	bi.Width = width
	bi.interval = interval
	bi.Parent = b.Internal()
	bi.Index = 0

	an := a.Internal()
	bn := b.Internal()
	an.Interfaces = append(an.Interfaces, ai)
	bn.Interfaces = append(bn.Interfaces, bi)

	l := &Link{
		Endpoints: [2]*Interface{ai, bi},
		Delay:     delay,
	}

	r.Links = append(r.Links, l)

	return l

}

func (r *Runtime) Run() {

	var done bool
	for {

		r.exec()
		r.Clock += 1

		select {
		case done = <-r.Stop:
		default:
		}

		if done {
			break
		}
	}

	log.Printf("elapsed: %d ps (%f) ms", r.Clock, float64(r.Clock)*1e-9)

}

func (r *Runtime) exec() {

	var wg sync.WaitGroup

	for _, c := range r.Components {
		wg.Add(1)
		go func(c Component) {
			if (r.Clock % c.Interval()) == 0 {
				c.Tick(r)
				for _, i := range c.Internal().Interfaces {
					if (r.Clock % i.Interval()) == 0 {
						i.Tick()
					}
				}
			}
			wg.Done()
		}(c)

	}

	for _, l := range r.Links {
		wg.Add(1)
		go func(l *Link) {
			l.Tock()
			wg.Done()
		}(l)
	}

	wg.Wait()

}

type Node struct {
	Id         string
	Interfaces []*Interface
}

type Component interface {
	Internal() *Node
	Interval() uint64
	Tick(*Runtime)
}

// Interface ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

type Interface struct {
	Parent        *Node
	Index         uint
	Width         uint
	In, Out       chan []byte
	Inbuf, Outbuf chan []byte

	interval uint64
}

const C int = 100000

func NewInterface() *Interface {

	return &Interface{
		In:     make(chan []byte, C),
		Out:    make(chan []byte, C),
		Inbuf:  make(chan []byte, C),
		Outbuf: make(chan []byte, C),
	}

}

func (i *Interface) Interval() uint64 { return i.interval }

func (i *Interface) Tick() {

	select {
	case data := <-i.Outbuf:
		i.Out <- data
		//log.Printf("iout %s%d (%d) %v", i.Parent.Id, i.Index, len(i.Out), data)
	default:
	}

	select {
	case data := <-i.Inbuf:
		i.In <- data
		//log.Printf("iin %s%d (%d) %v", i.Parent.Id, i.Index, len(i.In), data)
	default:
	}

}

func (i *Interface) PushOut(x []byte) {

	i.Outbuf <- x

}

func (i *Interface) PushIn(x []byte) {

	i.Inbuf <- x

}

func (i *Interface) PullIn() []byte {

	select {
	case data := <-i.In:
		return data
	default:
		return nil
	}

}

func (i *Interface) PullOut() []byte {

	select {
	case data := <-i.Out:
		return data
	default:
		return nil
	}

}

// Link +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

const R int = 1000000

type Link struct {
	Endpoints [2]*Interface
	Ring      [2][R]Ringlet
	Delay     uint64
	a, z      [2]int
}

func (l *Link) Tock() {

	xmit := func(r, a, b int) {

		//log.Printf("r[%d] a z: %d %d (%d)", r, l.a[r], l.z[r], l.a[r]-l.z[r])

		// push
		z := l.z[r]
		for i := z; i < l.a[r]; i++ {
			l.Ring[r][i].Delay -= 1
			if l.Ring[r][i].Delay <= 0 {
				l.Endpoints[b].PushIn(l.Ring[r][i].Data)
				l.z[r] = (l.z[r] + 1) % R
			}
		}

		// pull
		data := l.Endpoints[a].PullOut()
		if len(data) > 0 {
			l.Ring[r][l.a[r]] = Ringlet{l.Delay, data}
			l.a[r] = (l.a[r] + 1) % R
		}

	}

	// 0 -> 1 ~~~~~
	xmit(0, 0, 1)

	// 1 -> 0 ~~~~~
	xmit(1, 1, 0)

}

type Ringlet struct {
	Delay uint64
	Data  []byte
}
