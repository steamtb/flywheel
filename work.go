package main

import (
	"log"
)

type Worker struct {
	Node
	Send    bool
	Payload uint

	Stop func(*Worker) bool

	Tx, Rx uint
}

func (w *Worker) Internal() *Node {
	return &w.Node
}

func (w *Worker) Interval() uint64 {

	// 2.4ghz in picoseconds
	return uint64(1e12 / 2e9)

}

func (w *Worker) Tick(r *Runtime) {

	log.Printf("RX: %d", w.Rx)
	log.Printf("TX: %d", w.Tx)
	log.Printf("in: %d", len(w.Interfaces[0].In))
	log.Printf("inbuf: %d", len(w.Interfaces[0].Inbuf))
	log.Printf("out: %d", len(w.Interfaces[0].Out))
	log.Printf("outbuf: %d", len(w.Interfaces[0].Outbuf))

	for _, x := range w.Interfaces {

		if w.Send && w.Tx <= w.Payload {
			w.Tx += 1
			x.PushOut([]byte{47})
			log.Printf("%d", w.Tx)
		}

		if w.Stop != nil && w.Stop(w) {
			r.Stop <- true
			log.Printf("%s STOP", w.Id)
		}

		y := x.PullIn()
		if len(y) > 0 {
			w.Rx += uint(len(y))
			//log.Printf("%s in: %+v", w.Id, y)
		}

	}

}
