package main

import (
	"log"
)

func main() {

	log.SetFlags(0)

	r := NewRuntime()

	a := r.Add(&Worker{
		Node:    Node{Id: "a"},
		Send:    true,
		Payload: 1e4,
	})

	b := r.Add(&Worker{
		Node: Node{Id: "b"},
		Stop: func(w *Worker) bool {
			return w.Rx >= 1e4
		},
	})

	r.Connect(
		a, b,
		3000, // 3 nanosecond delay
		//1,
		16,  // width
		1e9, // 8 ghz
	)

	r.Run()

}
